
function makeAuthenticationChain (testMode, devSecurity, prodSecurity) {
    function authenticator (req, res, next) {
        var prodErr = null
        var mocknext = function (err, other) {
            if (err) {
                if (testMode) {
                    devSecurity(req, res, next)
                } else {
                    next(err)
                }
            } else {
                next()
            }
        }
        prodSecurity(req, res, mocknext)
    }

    return authenticator
}

module.exports = makeAuthenticationChain
