
var assert = require('assert')

var denied = (req, res, next) => {
    next('Denied')
}

var allow = (req, res, next) => {
    next()
}

describe('Given authentication chain factory', function () {
    var makeAuthenticationChain = require('./authenticationFactory.js')
    var scenarios = [
        {
            description: 'test mode and allowed by either authentication mode',
            expectDescription: 'user is allow to access',
            testmode: true,
            dev: allow,
            prod: allow,
            expect: null
        },
        {
            description: 'test mode and denied by production but allowed by development',
            expectDescription: 'user is allow to access',
            testmode: true,
            dev: allow,
            prod: denied,
            expect: null
        },
        {
            description: 'test mode and denied by production but denied by development',
            expectDescription: 'user is denied',
            testmode: true,
            dev: denied,
            prod: denied,
            expect: 'Denied'
        },
        {
            description: 'not test mode and allowed by production and development',
            expectDescription: 'user is allow to access',
            testmode: false,
            dev: allow,
            prod: allow,
            expect: null
        },
        {
            description: 'not test mode and denied by production and allowed development',
            expectDescription: 'user is denied',
            testmode: false,
            dev: allow,
            prod: denied,
            expect: 'Denied'
        },
        {
            description: 'not test mode and denied by production and denied development',
            expectDescription: 'user is denied',
            testmode: false,
            dev: denied,
            prod: denied,
            expect: 'Denied'
        }

    ]
    for (const s of scenarios) {
        describe(`When:  ${s.description}`, function () {
            var authenticator
            before(function () {
                authenticator = makeAuthenticationChain(s.testmode, s.dev, s.prod)
            })
            it(`Then expect: ${s.expectDescription}`, function (done) {
                authenticator({}, {}, (err) => {
                    assert.equal(s.expect, err)
                    done()
                })
            })
        })
    }
})
