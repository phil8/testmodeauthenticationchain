var jwt = require('jsonwebtoken')

var token = jwt.sign({ admin: 'true' }, 'secret2')

console.log(token)

var axios = require('axios')
var instance = axios.create({
    baseURL: 'http://localhost:3000/',
    headers: { 'Authorization': 'Bearer ' + token }
  })

function request () {
    instance.get('/protected')
}

async function doit () {
    await request()
}

doit()
