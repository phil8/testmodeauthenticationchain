var jwt = require('express-jwt')

const express = require('express')
const app = express()
const port = 3000

var devSecurity = jwt({ secret: 'secret' })
var prodSecurity = jwt({ secret: 'secret2' })

var makeAuthenticationChain = require('../index.js')

process.env.NODE_ENV = 'TestMode'

var authenticator = makeAuthenticationChain(process.env.NODE_ENV === 'TestMode', devSecurity, prodSecurity)

app.use(express.static('integration/public'))

app.get('/protected', authenticator,
  function (req, res) {
    if (!req.user.admin) return res.sendStatus(401)
    res.sendStatus(200)
  })

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
